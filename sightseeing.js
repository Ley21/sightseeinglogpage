
//const weatherPollingTimes = new Array(0,8,16,24);
//var lastPollingTime = -1;
//var jsonWheater = new Object();

const weatherUrl = "https://jsonp.nodejitsu.com/?url=http%3A%2F%2Fen.ff14angler.com%2Fskywatcher.php";

const LOCATIONS = new Array("Nothing", "Limsa Lominsa", "Middle La Noscea", 
"Lower La Noscea", "Eastern La Noscea", "Western La Noscea",
"Upper La Noscea", "Outer La Noscea", "Wolves' Den Pier", "Mist", 
"Gridania", "Central Shroud", "East Shroud", "South Shroud",
"North Shroud", "Lavender Beds", "Ul'dah", "Western Thanalan",
"Central Thanalan", "Eastern Thanalan", "Southern Thanalan", 
"Northern Thanalan", "The Goblet", "Coerthas Central Highlands",
"Mor Dhona");

const weatherImagePath = "img/weather/w0{0}.png";

const CHECKBOX_ID_PREFIX = "c_";
const TIME_FORMAT = "{0}:{1}";

const E_TIME = 20.5714285714;

const AVALIBLE_LOGS_TABLE_ID = "avalibleLogs";
const UPCOMMING_LOGS_TABLE_ID = "nextLogs";
const ALL_LOGS_TABLE_ID = "sightseeingTable";
const SKYWATCHER_TABLE_ID = "skywatcherTable";

const CELL_TEMPLATE = "<td id='{1}'>{0}</td>";
const SKYWATCH_NOW_CELL_ID = "L_{0}_NOW";
const SKYWATCH_NEXT_CELL_ID = "L_{0}_NEXT";
const REAL_TIME_CELL_ID = "RT_{0}";

const SIGHTSEEING_ROW_TEMPLATE = "<tr id='S_{0}'>{1}</tr>";

var global = {
    utcTime: null,
    eorzeaTime: null
    };
   

var LOGS;
var CHECKEDLOGS = new Object();

var WEATHER_DATA;
            
var DONE_IDS = new Array();

//Extensions
Number.prototype.toTwoDigitString = function(){
    var number = this;
    return number / 10 >= 1 ? number.toString() : "0" + number;
};

String.prototype.format = function() {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};

Array.prototype.remove = function(element){
    var array = this;
    var index = this.indexOf(element);
    if(index != -1){
        array.splice(index,1);
    }
    return array;
};

//Functions

function changeElementContent(elementId,newContent){
    $('#'+elementId).html(newContent);
    //document.getElementById(elementId).innerHTML = newContent;
}

function appandContent(elementId,content){
    $('#'+elementId).append(content);
    //document.getElementById(elementId).innerHTML += content;
}

function deleteElement(elementId){
    document.getElementById(elementId).outerHTML = "";
}

function elementExist(elementId){
    return document.getElementById(elementId) != null;
}

function getParentId(elementId){
    return document.getElementById(elementId).parentElement.id;
}

function clearTable(tableId){
    changeElementContent(tableId,"");
}

function orderTable(tableId, colIndex,sortFunc){
    var headRow = new Object();
    var rows = new Array();
    
    $('#'+tableId+" tr").each(function(i){
        if(i == 0){
            headRow = this;    
        }
        else{
            rows.push(this);
        }
    });
    var orderedRows = rows.sort(function(a,b){
        return sortFunc(a.cells[colIndex].innerHTML,
            b.cells[colIndex].innerHTML);
    });
    clearTable(tableId);
    appandContent(tableId,headRow.outerHTML);
    orderedRows.forEach(function(row){
        appandContent(tableId,row.outerHTML);
    });
    
}

var REVERSE_ORDER = new Array();

function orderById(tableId){
    order(tableId,0,function(a,b,reverse){
        var first = parseInt(a);
        var second = parseInt(b);
        if(!reverse){
            return  first - second;
        }
        else{
            return second - first;
        }
    });
}

function order(tableId,index,orderFunc){
    orderTable(tableId,index,function(a,b)
    {
        if(REVERSE_ORDER[index] === "undefined" 
            || !REVERSE_ORDER[index]){
                return  orderFunc(a,b,false);
        }
        else{
            return orderFunc(a,b,true);
        }
    });
    REVERSE_ORDER[index] = (REVERSE_ORDER[index] === "undefined" 
            || !REVERSE_ORDER[index]) ? true : false;
}

function orderByTime(tableId){
    order(tableId,5,function(a,b,reverse){
        var aValue = convertTimeArrayToSeconds(getTimeInfoArray(a));
        var bValue = convertTimeArrayToSeconds(getTimeInfoArray(b));
        if(!reverse){
            return  aValue - bValue;
        }
        else{
            return bValue - aValue;
        }
    });
}

function orderByString(tableId){
    order(tableId,1,function(a,b,reverse){
        if(!reverse){
            if(a < b) return -1;
            if(a > b) return 1;
            return 0;
        }
        else{
            if(a < b) return 1;
            if(a > b) return -1;
            return 0;
        }
    });
}

function getTimeInfoArray(timeString){
    var strngArray = timeString.split(":");
    var intArray = new Array();
    strngArray.forEach(function(s){
        intArray.push(parseInt(s));
    })
    return intArray;
}

function convertTimeArrayToSeconds(timeArray){
    return (timeArray[0] * 60 + timeArray[1] )* 60 + timeArray[2];
}

function showTime() {
    var hours = global.eorzeaTime.getUTCHours();
    var minutes = global.eorzeaTime.getUTCMinutes();
    var timeString = TIME_FORMAT.format(hours.toTwoDigitString(),
        minutes.toTwoDigitString());
    changeElementContent("clock",timeString);
}

function updateClock() {
    global.utcTime = new Date().getTime();
    var eo_timestamp = Math.floor(global.utcTime * E_TIME);
    global.eorzeaTime = new Date();
    global.eorzeaTime.setTime(eo_timestamp);
}


function duration (timeHour){
    var hours = global.eorzeaTime.getUTCHours();
    var minutes = global.eorzeaTime.getUTCMinutes();
    var diffrence = hours < timeHour ? 
        timeHour - hours : 24 - hours + timeHour;
    return (diffrence - 1) * 60 + 60 - minutes;
    
}
    
function eozToReal (minutes){
    return (1/20 * minutes);
}

function createTableHeadline(tableId,extraHeadline){
    var table = "<tr>";
    table += "<th onclick='orderById(\""+tableId+"\")'><a>ID</a></th>";
    table += "<th onclick='orderByString(\""+tableId+"\")'><a>Location</a></th>";
    table += "<th>Coordinate</th>";
    table += "<th colspan='2' >Weather</th>";
    table += "<th>Time</th>";
    if (typeof(extraHeadline) === "undefined"){
        table += "<th onclick='orderByTime(\""+tableId+"\")'><a>" + extraHeadline + "</a></th>";
    }
    table += "<th>Emote</th>";
    table += "<th>Done</th>";
    table += "</tr>";
    return table;
}

function initTables(){
    changeElementContent(AVALIBLE_LOGS_TABLE_ID,createTableHeadline(AVALIBLE_LOGS_TABLE_ID,"Remaining Time"));
    changeElementContent(UPCOMMING_LOGS_TABLE_ID,createTableHeadline(UPCOMMING_LOGS_TABLE_ID,"Upcomming Time"));
    createAllSightseeingTable();
    initSkywatcherTable();
}

function createImage(weatherId){
    return "<img src='" + getWeatherImagePath(weatherId)
        + "' alt='Responsive image'>";
}

function createImageCell(weatherIds){
    //var cell = "<table><tr>";
    var cell = "<td colspan='2' >";
    weatherIds.forEach(function(w){
        cell += createImage(w);
    });
    cell += "</td>";
    
    
    return cell;
}

function createAllSightseeingTable(){
    var table = createTableHeadline(ALL_LOGS_TABLE_ID);
    LOGS.forEach(function(l){
        table += "<tr>";
        table += CELL_TEMPLATE.format(l.number);
        table += CELL_TEMPLATE.format(LOCATIONS[l.location]);
        table += CELL_TEMPLATE.format(l.coordinate);
        table += createImageCell(l.weather);
        
        var timeCell = TIME_FORMAT.format(l.startTime.toTwoDigitString(),"00") 
            + " - " + TIME_FORMAT.format(l.endTime.toTwoDigitString(),"00");
        table += CELL_TEMPLATE.format(timeCell);
        table += CELL_TEMPLATE.format("<kbd>" + "/" + l.emote + "</kbd>");
        var checkboxId = CHECKBOX_ID_PREFIX + l.number;
        table += "<td><input id='{0}' type='checkbox' class='checkbox' {1}></td>".format(
            checkboxId, DONE_IDS.indexOf(l.number) != -1 ? "checked" : "");
        table += "</tr>";
    });
    changeElementContent(ALL_LOGS_TABLE_ID,table);
    //$('.checkbox').checkbox();
}


function initSkywatcherTable(){
    var table = "<tr>";
    table += "<th>Location</th>";
    table += "<th>Now</th>";
    table += "<th>Next</th>";
    table += "</tr>";
    var i = 1;
    LOCATIONS.forEach(function(l){
        if(l == "Nothing"){
            return;
        }
        table += "<tr>";
        table += CELL_TEMPLATE.format(l);
        table += CELL_TEMPLATE.format("",SKYWATCH_NOW_CELL_ID.format(i));
        table += CELL_TEMPLATE.format("",SKYWATCH_NEXT_CELL_ID.format(i));
        table += "</tr>";
        i++;
    });
    changeElementContent(SKYWATCHER_TABLE_ID,table);
}

function updateSkywatchTable(){
    var i = 1;
    LOCATIONS.forEach(function(l){
        if(l == "Nothing"){
            return;
        }
        var watherIdNow = getWeatherId(i,0);
        var watherIdNext = getWeatherId(i,1);
        
        changeElementContent(SKYWATCH_NOW_CELL_ID.format(i),
            createImage(watherIdNow));
        changeElementContent(SKYWATCH_NEXT_CELL_ID.format(i),
            createImage(watherIdNext));
        
        i++;
    });
}

function getWeatherId(locationId,time){
    
    var filteredLogEntry = WEATHER_DATA.data.filter(function(w){
        return (w.area == locationId && w.time == time);
    })[0];
    
    return filteredLogEntry !== undefined ? filteredLogEntry.weather : 0;
}

function getWheater(hour,location){
    var currentHour = global.eorzeaTime.getUTCHours();
    
    var timeToWeather = 8 - currentHour % 8;
    var timeDiffrence = hour - currentHour;
    if (timeDiffrence < 0){
       timeDiffrence += 24;
    }
    
    var forcast = timeDiffrence - timeToWeather < 0 ? 0 : Math.floor((timeDiffrence + currentHour % 8) / 8);
        
    return getWeatherId(location,forcast);
}

function getEndTime(endTime,weather,location){
    var hour = global.eorzeaTime.getUTCHours();
    var upcommingWeather = getWheater(endTime,location);
    if(weather.indexOf(upcommingWeather) != -1){
        return endTime;
    }
    else{
        var timeToWeather = 8 - hour % 8;
        return hour + timeToWeather;
    }
}

function getStartTime(startTime,weather,location){
    var upcommingWeather = getWheater(startTime,location);
    return weather.indexOf(upcommingWeather) != -1 ? startTime : -1;
}

function checkLogs(){
    CHECKEDLOGS = new Array();
    LOGS.forEach(function(l){
        var timeHour = global.eorzeaTime.getUTCHours();
        var weather = getWheater(timeHour,l.location);
        var avalible = l.weather.indexOf(weather) != -1 
            && inTime(timeHour,l.startTime,l.endTime);
        var switchTime = avalible ? getEndTime(l.endTime,l.weather,l.location) :
            getStartTime(l.startTime,l.weather,l.location);
        CHECKEDLOGS.push({log: l, avalible: avalible, switchTime: switchTime});
    });
}

function createSightseeingRow(log){
    var rowContent = "";
    rowContent += CELL_TEMPLATE.format(log.number);
    rowContent += CELL_TEMPLATE.format(LOCATIONS[log.location]);
    rowContent += CELL_TEMPLATE.format(log.coordinate);
    rowContent += createImageCell(log.weather);
    
    var timeCell = TIME_FORMAT.format(log.startTime.toTwoDigitString(),"00") 
        + " - " + TIME_FORMAT.format(log.endTime.toTwoDigitString(),"00");
    rowContent += CELL_TEMPLATE.format(timeCell);
    rowContent += CELL_TEMPLATE.format("",REAL_TIME_CELL_ID.format(log.number));
    rowContent += CELL_TEMPLATE.format("<kbd>" + "/" + log.emote + "</kbd>");
    
    var checkboxId = CHECKBOX_ID_PREFIX + log.number;
    rowContent += "<td><input id='{0}' type='checkbox' class='checkbox' onclick='checkboxOnClick(this)' {1}></td>".format(
        checkboxId, DONE_IDS.indexOf(log.number) != -1 ? "checked" : "");
    return SIGHTSEEING_ROW_TEMPLATE.format(log.number,rowContent);
}

function modifyTablesTables(){
    CHECKEDLOGS.forEach(function(e){
        var rowId = "S_" + e.log.number;    
        var inTable = elementExist(rowId);
        
        var avalibleGlobal = e.switchTime != -1;
        switch (showState) {
            case 1:
                avalibleGlobal = avalibleGlobal & (e.log.number <= 20);
                break;
            case 2:
                avalibleGlobal = avalibleGlobal & (e.log.number > 20);
        }
        var toDoCheck = checkToDo(e.log.number);
        avalibleGlobal = avalibleGlobal & toDoCheck;
        if(!inTable){
            if(!avalibleGlobal){
                return;
            }
        }
        else{
            if(!avalibleGlobal){
                deleteElement(rowId);
                return;
            }
            else{
                var inAvalible = getParentId(rowId) == AVALIBLE_LOGS_TABLE_ID;
                if((e.avalible && !inAvalible) 
                    || (!e.avalible && inAvalible)){
                    deleteElement(rowId);
                }
                else{
                    return;
                }
                
            }
        }
        appandContent(e.avalible ? AVALIBLE_LOGS_TABLE_ID 
                    : UPCOMMING_LOGS_TABLE_ID,createSightseeingRow(e.log));
        //$('#'+CHECKBOX_ID_PREFIX + e.log.number).checkbox();
    });
}


function updateRealTimeCell(){
    const TiME = "{0}:{1}:{2}";
    CHECKEDLOGS.forEach(function(e){
        if(e.switchTime != -1){
            var timeUntil = duration(e.switchTime);
            var realTimeDuration = eozToReal(timeUntil);
            var hours = Math.floor(realTimeDuration / 60);
            var minutes = Math.floor(realTimeDuration % 60);
            var seconds = Math.floor((realTimeDuration - minutes ) * 60);
            
            var timeString = TiME.format(hours.toTwoDigitString(),
                minutes.toTwoDigitString(), seconds.toTwoDigitString());
            changeElementContent(REAL_TIME_CELL_ID.format(e.log.number),
                timeString);
        }
    });
}


function checkToDo(number){
    switch(showToDo){
        case 0:
            return true;
        case 1:
            return DONE_IDS.indexOf(number) == -1;
        case 2:
            return DONE_IDS.indexOf(number) != -1;
    }
}

var showState = 0;
var showToDo = 0;
//var previousMin = -1;

/*
function serialize(object) {
    object['#'] = object.constructor.name;
    return JSON.stringify(object);
}

function deserialize(string) {
    var object = JSON.parse(string);
    object.__proto__ = window[object['#']].prototype;
    return object;
}
*/
/**
 * Check if a time is in range.
 * @param {int} hour The hour to check in between the time range.
 * @param {int} startTime Start of the time range.
 * @param {int} endTime End of the time range.
 * @return {boolean} Return true if is in range.
 */
function inTime(hour,startTime,endTime){
    return (startTime > endTime ? (endTime > hour || startTime <= hour):
        (startTime <= hour &&  endTime > hour ));
}




function getWeather(){
    $.getJSON(weatherUrl).done(function(d){
        WEATHER_DATA = d;
    });
}



function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

const COOKIE_NAME = "entries";

function saveEntries() {
    setCookie(COOKIE_NAME,DONE_IDS.toString(),180);
}

function getEntries(){
    var cookie = getCookie(COOKIE_NAME);
    var entries = cookie.split(",");
    
    if(cookie != ""){
        entries.forEach(function(e){
            DONE_IDS.push(parseInt(e));
        });
    }
}



function getWeatherImagePath(weatherId){
    return weatherImagePath.format(weatherId / 10 >= 1 ? weatherId : "0" + weatherId);
}

function checkEntry(element,stringId){
    var id = parseInt(stringId);
    if(element.checked) {
        DONE_IDS.push(id);
    }
    else{
        DONE_IDS.remove(id);
    }
    saveEntries();
}

var previousHour = -1;

function pollingEveryNewHour(pollingCallback){
    var hour = global.eorzeaTime.getUTCHours();
    if(previousHour != hour){
        recheckWeather(hour,previousHour == -1 ? true : false);
        pollingCallback();
        previousHour = hour;
    }
}

function recheckWeather(hour,force){
    if (typeof(force) === "undefined") {force = false;}
    if(hour % 8 == 0 || force){
        getWeather();
        updateSkywatchTable();
    }
}

function polling(){
    updateClock();
    showTime();
    pollingEveryNewHour(function(){
        checkLogs();
        modifyTablesTables();
    });
    updateRealTimeCell();
}




function getLogs(){
    
    $.ajaxSetup({
        async: false
    });
    $.getJSON("sigthseeingLogs.json").done(function(data){
        LOGS = data;
    });
}

getEntries();

updateClock();
getLogs();
getWeather();
checkLogs();

initTables();
getEntries();

setInterval(polling,1000);


//Click functions
$('#all').on('click', function(){
    $('#last').removeClass('active');
    $('#first').removeClass('active');
    showState = 0;
    modifyTablesTables();
}); 
$('#first').on('click', function(){
    $('#last').removeClass('active');
    $('#all').removeClass('active');
    showState = 1;
    modifyTablesTables();
}); 
$('#last').on('click', function(){
    $('#all').removeClass('active');
    $('#first').removeClass('active');
    showState = 2;
    modifyTablesTables();
}); 


$('#ignoreState').on('click', function(){
    $('#done').removeClass('active');
    $('#todo').removeClass('active');
    showToDo = 0;
    modifyTablesTables();
}); 
$('#todo').on('click', function(){
    $('#done').removeClass('active');
    $('#ignoreState').removeClass('active');
    showToDo = 1;
    modifyTablesTables();
}); 
$('#done').on('click', function(){
    $('#ignoreState').removeClass('active');
    $('#todo').removeClass('active');
    showToDo = 2;
    modifyTablesTables();
}); 

function checkboxOnClick(elem){
    var checkBoxId = $(elem).attr('id');
    var id = parseInt(checkBoxId.replace("c_",""));
    var checked = elem.checked;
    $('#'+checkBoxId + ":checked").val(checked);
    
    if(checked){
        DONE_IDS.push(id);
    }
    else{
        DONE_IDS.remove(id);
    }
    saveEntries();
}
$('.checkbox').on('click', function(){
    checkboxOnClick(this);
}); 
