# FFXIV Sightseeing Page #

This is a page for all people who do not want to get crazy by looking for the right weather and time every 8 hours at the InGame NPC. It shows you the sightseeing point whitch currently avalible and the time until it disappiers again.

### What can this page do? ###

* Show you Eorzea Time
* Show a list of all Sightseeing Points and Weather
* Current wheather in each location
* List all current avalible Points and all upcomming

### How do it work? ###

* The time will calculate by convert GMT time to EOZ time by const "20.5714285714"
* All nodes a list in a json file
* The weather information are from http://www.en.ff14angler.com/

### Used external frameworks ###

* Bootstrep framework

### Some other information ###

Because I'm not a professional Javascript and HTML/CSS Developer this page maybe have some bugs or else. If you are think you can do it better and create a nice fork I will try to upgrade this with your ideas. Some features are missing and I hope I can add them in the next time.

### Knowing Missing Features ###

* For each point a hint like on http://vistas.explorexiv.com/
* Better sorting options for tables
* Sound alarm for selected points